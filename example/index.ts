import {Feature, Server} from '../src';


async function main(): Promise<void>
{
	const server = new Server('rsc-test', [
		Feature.walkingOrRunningStatus,
		Feature.totalDistance,
	]);

	await server.init();

	let distance = 100;

	setInterval(() => {
		const speed = Math.random() * (10 - 5) + 5;

		server.send({
			instantaneousSpeed: speed / 3.6,
			instantaneousCadence: 5,
			totalDistance: distance,
			running: speed >= 7.5,
		});

		distance++;
	}, 2000);
}

main().catch(err => console.error(err));
