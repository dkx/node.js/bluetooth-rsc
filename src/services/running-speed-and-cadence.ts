import {RscFeature, RscMeasurement, RscMeasurementData} from "../characteristics";
import {Feature} from "../server";
import {Service} from "./service";

const bleno = require('@abandonware/bleno');


/**
 * org.bluetooth.service.running_speed_and_cadence
 * https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Services/org.bluetooth.service.running_speed_and_cadence.xml
 */
export class RunningSpeedAndCadence implements Service
{
	private readonly service: any;

	private readonly rscMeasurement = new RscMeasurement();

	constructor(features: Array<Feature>)
	{
		this.service = new bleno.PrimaryService({
			uuid: '1814',
			characteristics: [
				new RscFeature(features).toInternalCharacteristic(),
				this.rscMeasurement.toInternalCharacteristic(),
			],
		});
	}

	public toInternalService(): any
	{
		return this.service;
	}

	public sendRscMeasurement(data: RscMeasurementData): void
	{
		this.rscMeasurement.send(data);
	}
}
