import {RunningSpeedAndCadence} from "./services";

const bleno = require('@abandonware/bleno');


export enum Feature
{
	totalDistance,
	walkingOrRunningStatus,
}

export declare interface Data
{
	instantaneousSpeed: number,
	instantaneousCadence: number,
	totalDistance?: number,
	running?: boolean,
}

export class Server
{
	private readonly runningSpeedAndCadence: RunningSpeedAndCadence;

	constructor(
		private readonly name: string,
		private readonly features: Array<Feature> = [],
	) {
		this.runningSpeedAndCadence = new RunningSpeedAndCadence(features);
	}

	public async init(): Promise<void>
	{
		const handler = (state: string) => {
			if (state === 'poweredOn') {
				bleno.startAdvertising(this.name, ['1814'], (err: Error) => {
					if (err) {
						throw err;
					}
				});
			} else {
				bleno.removeListener('stateChange', handler);
				bleno.stopAdvertising();
			}
		};

		bleno.on('stateChange', handler);

		return new Promise((resolve, reject) => {
			bleno.once('advertisingStart', (err: Error) => {
				if (err) {
					return reject(err);
				}

				const services = [
					this.runningSpeedAndCadence.toInternalService(),
				]

				bleno.setServices(services, (err: Error) => {
					if (err) {
						return reject(err);
					}

					resolve();
				});
			});
		});
	}

	public send(data: Data): void
	{
		this.runningSpeedAndCadence.sendRscMeasurement({
			instantaneousSpeed: data.instantaneousSpeed,
			instantaneousCadence: data.instantaneousCadence,
			totalDistance: data.totalDistance,
			running: data.running,
		});
	}
}
