const bleno = require('@abandonware/bleno');


/**
 * org.bluetooth.descriptor.gatt.client_characteristic_configuration
 * https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Descriptors/org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
 */
export class ClientCharacteristicConfiguration extends bleno.Descriptor
{
	constructor()
	{
		super({
			uuid: '2902',
			value: Buffer.alloc(2),
		});
	}
}
