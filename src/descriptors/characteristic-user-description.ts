const bleno = require('@abandonware/bleno');


/**
 * org.bluetooth.descriptor.gatt.characteristic_user_description
 * https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Descriptors/org.bluetooth.descriptor.gatt.characteristic_user_description.xml
 */
export class CharacteristicUserDescription extends bleno.Descriptor
{
	constructor(description: string)
	{
		super({
			uuid: '2901',
			value: description,
		});
	}
}
