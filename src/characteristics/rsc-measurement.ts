import {CharacteristicUserDescription, ClientCharacteristicConfiguration} from "../descriptors";
import {Characteristic} from "./characteristic";

const bleno = require('@abandonware/bleno');


export declare interface RscMeasurementData
{
	instantaneousSpeed: number,
	instantaneousCadence: number,
	totalDistance?: number,
	running?: boolean,
}

/**
 * org.bluetooth.characteristic.rsc_measurement
 * https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.rsc_measurement.xml
 */
export class RscMeasurement implements Characteristic
{
	private readonly characteristic: any;

	private sender: (data: Buffer) => void = () => {};

	constructor()
	{
		this.characteristic = new bleno.Characteristic({
			uuid: '2A53',
			properties: ['notify'],
			value: null,
			descriptors: [
				new CharacteristicUserDescription('Running Speed And Cadence'),
				new ClientCharacteristicConfiguration(),
			],
			onSubscribe: (maxValueSize: number, updateValue: (data: Buffer) => void) => {
				this.sender = updateValue;
			},
		});
	}

	public toInternalCharacteristic(): any
	{
		return this.characteristic;
	}

	public send(data: RscMeasurementData): void
	{
		const value = Buffer.alloc(10);
		let offset = 0;

		let flags = 0b00000000;

		if (typeof data.totalDistance !== 'undefined') {
			flags |= 0b00000010;
		}

		if (data.running === true) {
			flags |= 0b00000100;
		}

		// Flags
		value.writeUInt8(flags, offset);
		offset += 1;

		// Instantaneous Speed
		value.writeUInt16LE(data.instantaneousSpeed * 256, offset);
		offset += 2;

		// Instantaneous Cadence
		value.writeUInt8(Math.min(data.instantaneousCadence, 255), offset);
		offset += 1;

		// Total Distance
		if (typeof data.totalDistance !== 'undefined') {
			value.writeUInt32LE(data.totalDistance, offset);
			offset += 4;
		}

		this.sender(value.slice(0, offset));
	}
}
