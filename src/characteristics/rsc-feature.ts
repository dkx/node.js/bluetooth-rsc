import {CharacteristicUserDescription} from "../descriptors";
import {Feature} from "../server";
import {Characteristic} from "./characteristic";

const bleno = require('@abandonware/bleno');


/**
 * org.bluetooth.characteristic.rsc_feature
 * https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.rsc_feature.xml
 */
export class RscFeature implements Characteristic
{
	private readonly characteristic: any;

	constructor(features: Array<Feature>)
	{
		let bits = 0b00000000;

		if (features.indexOf(Feature.totalDistance) >= 0) {
			bits |= 0b00000010;
		}

		if (features.indexOf(Feature.walkingOrRunningStatus) >= 0) {
			bits |= 0b00000100;
		}

		this.characteristic = new bleno.Characteristic({
			uuid: '2A54',
			properties: ['read'],
			value: Buffer.from([bits, 0]),
			descriptors: [
				new CharacteristicUserDescription('RSC Feature'),
			],
		});
	}

	public toInternalCharacteristic(): any
	{
		return this.characteristic;
	}
}
